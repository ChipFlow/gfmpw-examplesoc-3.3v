# ChipFlow example SOC on gf180mcu with 3.3V version of c4m-flexcell

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This is the [example-socs](https://gitlab.com/ChipFlow/example-soc) design
P&R with 3.3V standard cells for GFMPW.
[pnr_socs repo](https://gitlab.com/ChipFlow/pnr_socs) contains the
open source flow to regenerate this gds file.

As there is only one vdd for the whole chip, this design assumes one can run
the design at a vdd somewhere between 3.3V and 5V that allows to have both the
IO and harness functioning properly and not destroying the 3.3V logic gates.

